extends KinematicBody

onready var camera = $Pivot/ClippedCamera

var gravity = -30
var max_speed = 8
var sens = 0.002 # rad/pixel
var jump_str = 10
var grounded = false

var velocity = Vector3()

func jump():
	if grounded:
		grounded = false
		velocity += Vector3.UP * jump_str

func get_input():
	var input_dir = Vector3()

	if Input.is_action_pressed("move_forward"):
		input_dir += -global_transform.basis.z
	if Input.is_action_pressed("move_back"):
		input_dir += global_transform.basis.z
	if Input.is_action_pressed("strafe_left"):
		input_dir += -global_transform.basis.x
	if Input.is_action_pressed("strafe_right"):
		input_dir += global_transform.basis.x
	if Input.is_action_pressed("jump"):
		jump()
	input_dir = input_dir.normalized()
	return input_dir

func _unhandled_input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		rotate_y(-event.relative.x * sens)
		$Pivot.rotate_x(-event.relative.y * sens)
		$Pivot.rotation.x = clamp($Pivot.rotation.x, -1.2, 1.2)

func _physics_process(delta):
	velocity.y += gravity * delta
	var desired_velocity = get_input() * max_speed

	velocity.x = desired_velocity.x
	velocity.z = desired_velocity.z
	velocity = move_and_slide(velocity, Vector3.UP, true)

	if is_on_floor():
		grounded = true

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
